import 'package:flutter/material.dart';

class Application extends StatefulWidget {
    static ApplicationState CurrentApplicationState = ApplicationState();

    const Application({required Key key}) : super(key: key);

    static void restart() {
        // print("Application.Restart");
        CurrentApplicationState.restartApp();
    }

    @override
    State<Application> createState() => CurrentApplicationState = ApplicationState();
}

class ApplicationState extends State<Application> {
    Key key = UniqueKey();

    void restartApp() {
        setState(() {
            // print("ApplicationState.restartApp");
            key = UniqueKey();
        });
    }

    @override
    Widget build(BuildContext context) => KeyedSubtree(
        key: key,
        child: MaterialApp(
            // locale: Locale('vi', 'vi'),
            debugShowCheckedModeBanner: false,
            title: 'SLaiXe',
            theme: ThemeData(
            brightness: Brightness.light,
            scaffoldBackgroundColor: Colors.white,
            colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue)
                .copyWith(secondary: Colors.brown),
            ),
            home: Container()
        )
    );
}