# Ghi chú phát triển Flutter

## Khởi tạo dự án
Việc khởi tạo dự án ưu tiên sử dụng công cụ dòng lệnh mặc định của flutter, được cài đặt chính thống từ website flutter.dev

Khi khởi tạo dự án, chạy dòng lệnh sau trong công cụ dòng lệnh tại thư mục chứa dự án.

```sh
flutter create sample --org app.dk198 --description "ứng dụng mẫu có id là app.dk189.sample cho nền tảng ios và android" --platforms "ios,android" --ios-language swift --android-language java
```

**Công cụ dòng lệnh thường là Terminal (Windows, Linux, MacOS) hoặc CMD, PowerShell (Windows).*

**Thư mục chứa dự án thường là thư mục được liên kết với kho lưu trữ GIT do bộ phận quản lý thiết lập.*

**Giải thích câu lệnh trên**

- Sau khi chạy dòng lệnh trên, flutter sẽ tạo 1 thư mục mới có tên là sample, tương ứng với tham số dòng lệnh theo sau lệnh `flutter create`. Đây cũng là tên phát triển của ứng dụng sẽ được khởi tạo mặc định trong tệp `pubspec.yaml`.
- Tiếp đến tham số `--org` đại diện cho mã tổ chức phát hành ứng dụng, hiểu nôm na là tiền tố cho package id của ứng dụng, thường được đặt bằng quy tắc miền đảo (reverse domain). Chẳng hạn tổ chức (công ty, cá nhân) có miền (tên miền) là sonphatgroup.vn, vậy thì khi viết sẽ là vn.sonphatgroup. Nếu công ty tạo ứng dụng có tên phát triển là sticketsale, vậy thì id ứng dụng sẽ có kết quả là vn.sonphatgroup.sticketsale. nếu công ty xác định dùng domain là sbus.vn thì kết quả sẽ là vn.sbus.sticketsale. Tương tự với ví dụ trên, Trọng sử dụng domain là dk189.app và tên phát triển của ứng dụng là sample, vậy kết quả sẽ là app.dk189.sample
- Tham số `--description` để chỉ định một số nội dung mô tả về ứng dụng, nên ghi những thông tin cơ bản để người sau dễ hiểu về mục đích phát triển ứng dụng, hoặc thông tin đặc thù về quá trình phát triển.
- Tham số `--platforms` để chỉ định các nền tảng đích mà dự án này hướng đến. Có thể đề cập nhiều mã nền tảng, phân cách nhau bởi dấu `, (dấu phẩy)`. Thời điểm thực hiện bài viết này (2022-08-30), Flutter hỗ trợ chính thức các nền tảng có mã là `android,ios,web,linux,macos,windows`. Như ví dụ trên sẽ tạo dự án với các nền tảng đích là `ios` và `android`.
- Tham số `--ios-language` để chỉ định ngôn ngữ 'gốc' sử dụng cho ứng dụng dành cho ios. Hỗ trợ 2 ngôn ngữ là Swift và Object-C với mã tương ứng là `swift` và `objc`.
- Tham số `--android-language` để chỉ định ngôn ngữ 'gốc' sử dụng cho ứng dụng dành cho android. Hỗ trợ 2 ngôn ngữ là Java và Kotlin với mã tương ứng là `java` và `kotlin`.

**Kết quả cuối cùng là dự án `sample` do flutter tạo ra có mã là `app.dk189.sample`, hướng đến 2 nền tảng là `android` và `ios` tương ứng với phần mã gốc đính kèm là `java` và `swift`.**

## Cấu trúc dự án

```text
~/sample/
├── README.md
├── analysis_options.yaml
├── android => thư mục mặc định theo flutter platform cho android, chứa mã gốc cho android
│   ├── ...
├── assets => chứa các tài nguyên dùng trong ứng dụng
│   ├── images
│   │   ├── ...
│   ├── fonts
│   │   ├── ...
│   ├── ...
├── ios => thư mục mặc định theo flutter platform cho ios, chứa mã gốc cho ios
│   ├── ...
├── lib => thư mục mặc định chứa mã nguồn dự án
│   ├── Helpers => <chứa các thành phần phục vụ hỗ trợ lập trình. ex: ApiHelper, LoginHelper,...>
│   │   ├── api_helper.dart
│   │   ├── format_helper.dart
│   │   └── login_helper.dart
│   │   └── ...
│   ├── Models => <chứa các mô hình thành phần dữ liệu. ex: LoginModel, RegisterModel,...>
│   │   ├── du_lieu_dang_nhap_model.dart
│   │   ├── du_lieu_trang_chinh_model.dart
│   │   └── thong_tin_ca_nhan_model.dart
│   │   └── ...
│   ├── Services => <chứa các thành phần xử lý nghiệp vụ kết nối đến back-end, hoặc xử lý chức năng thiết bị>
│   │   ├── bieu_do_tong_quan_service.dart
│   │   └── thong_tin_tai_khoan_service.dart
│   │   └── ...
│   ├── Views => chứa các thành phần giao diện
│   │   ├── HoSoCaNhan
│   │   │   ├── doi_mat_khau.dart
│   │   │   ├── ho_so_ca_nhan.dart
│   │   │   └── sua_thong_tin.dart
│   │   ├── TrangChinh
│   │   │   ├── Components
│   │   │   │   └── bieu_do.dart
│   │   │   ├── popup_thong_tin_chi_tiet.dart
│   │   │   └── trang_chinh.dart
│   │   └── ...
│   │   └── application.dart
│   └── main.dart => chỉ chứa hàm `void main();` và một số các hàm liên quan đến việc khởi động ứng dụng
├── pubspec.lock
├── pubspec.yaml
├── sample.iml
└── test => <kịch bản test tự động>
    └── widget_test.dart
```

Cấu trúc thông thường của một dự án flutter bao gồm các thư mục là:
- assets: chứa các tài nguyên dùng trong ứng dụng
- lib: chứa mã nguồn dự án
- test: chứa mã nguồn kịch bản test tự động
- ios,android,web,...: là thư mục mặc định do flutter tạo ra cho các nền tảng tương ứng. Chứa mã gốc và thông tin dự án ứng dụng cho nền tảng đích.

Để quá trình phát triển diễn ra thuận lợi, tối ưu, dễ bảo trì trong tương lai, áp dụng một số quy tắc sau cho phân cấp cấu trúc.
- Trong thư mục lib tạo các thư mục sau: Views, Services, Helpers, Models
- Sau đó bố trí các tệp mã nguồn vào các vị trí tương ứng, như đã mô tả ở ví dụ trên.
- Các thành phần class trong thư mục Models cần bổ xung hậu tố là Model, tên file có hậu tố là _model.dart
- Các thành phần class trong thư mục Services cần bổ xung hậu tố là Service, tên file có hậu tố là _service.dart
- Các thành phần class trong thư mục Helpers cần bổ xung hậu tố là Helper, tên file có hậu tố là _help.dart
- Các thành phần Views không cần phải có hậu tố, có cũng không sao.
- Ngoài ra, bên trong các thư mục trên có thể tạo thêm các thư mục con, nhằm mục đích nhóm các nghiệp vụ liên quan hoặc nhóm các thành phần liên quan lại với nhau.